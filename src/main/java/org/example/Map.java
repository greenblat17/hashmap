package org.example;

import java.util.Collection;
import java.util.Set;

public interface Map<K, V> {

    V put(K key, V value);

    V get(Object key);

    Set<K> keySet();

    Collection<V> values();

    boolean containsKey(Object key);

    boolean containsValue(Object value);

    boolean isEmpty();

    V remove(Object key);

    int size();

    void clear();
}
