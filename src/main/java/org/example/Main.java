package org.example;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < 100; i++) {
            map.put(String.format("%d", i), i);
        }
        for (int i = 0; i < 100; i++) {
            map.remove(String.format("%d", i));
            if (map.size() == 88)
                System.out.println();
        }
    }
}