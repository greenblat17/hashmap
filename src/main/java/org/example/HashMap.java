package org.example;


import java.util.*;

public class HashMap<K, V> implements Map<K, V> {


    private static final int DEFAULT_CAPACITY = 16;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;


    private static class Node<K, V> {
        final int hash;
        final K key;
        V value;
        Node<K, V> next;

        Node(int hash, K key, V value, Node<K, V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node<?, ?> node = (Node<?, ?>) o;
            return hash == node.hash && Objects.equals(key, node.key) && Objects.equals(value, node.value) && Objects.equals(next, node.next);
        }

        @Override
        public int hashCode() {
            return Objects.hash(hash, key, value, next);
        }
    }


    Node<K, V>[] table;
    int size;
    int threshold;
    float loadFactor;

    private Collection<V> values;
    private Set<K> keySet;


    public HashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0 || loadFactor < 0) {
            throw new IllegalArgumentException();
        }
        this.loadFactor = loadFactor;
        this.threshold = (int) (initialCapacity * loadFactor);
    }

    public HashMap() {
        this(DEFAULT_CAPACITY, DEFAULT_LOAD_FACTOR);
    }

    public HashMap(int initialCapacity) {
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

    static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }


    @Override
    public V put(K key, V value) {
        int hash = hash(key);

        var kvNode = putVal(hash, key, value);
        afterNodeInsertion(kvNode);

        if (size > threshold)
            resize();

        return value;
    }

    private Node<K, V> putVal(int hash, K key, V value) {
        int n;
        if (table == null || (n = table.length) == 0) {
            table = resize();
            n = table.length;
        }

        int idx = (n - 1) & hash;
        Node<K, V> first;

        Node<K, V> nodeToAdd;
        if ((first = table[idx]) == null) {
            nodeToAdd = newNode(hash, key, value, null);
            table[idx] = nodeToAdd;
            size++;

            return nodeToAdd;
        } else {
            Node<K, V> tmp = first;
            while (true) {
                if (tmp.hash == hash && ((Objects.equals(key, tmp.key)))) {
                    tmp.value = value;
                    nodeToAdd = tmp;
                    return nodeToAdd;
                }

                if (tmp.next == null) {
                    nodeToAdd = newNode(hash, key, value, null);
                    tmp.next = nodeToAdd;
                    size++;
                    return nodeToAdd;
                }

                tmp = tmp.next;
            }
        }
    }

    private void afterNodeInsertion(Node<K, V> nodeToAdd) {
        if (values == null)
            values = new ArrayList<>();
        values.add(nodeToAdd.value);

        if (keySet == null)
            keySet = new HashSet<>();
        keySet.add(nodeToAdd.key);

    }

    private void afterNodeDeleting(Node<K, V> nodeToDelete) {
        if (nodeToDelete != null) {
            values.remove(nodeToDelete.value);
            keySet.remove(nodeToDelete.key);
        }
    }


    private Node<K, V> newNode(int hash, K key, V value, Node<K, V> next) {
        return new Node<>(hash, key, value, next);
    }

    private Node<K, V>[] resize() {
        if (table == null) {
            return (Node<K, V>[]) new Node[DEFAULT_CAPACITY];
        }

        Node<K, V>[] tab = table;
        table = (Node<K, V>[]) new Node[size * 2];
        for (Node<K, V> kvNode : tab) {
            Node<K, V> node = kvNode;
            if (node != null) {
                while (node != null) {
                    Node<K, V> next = node.next;
                    node.next = null;

                    int idx = (table.length - 1) & node.hash;
                    if (table[idx] == null)
                        table[idx] = node;
                    else {
                        Node<K, V> tmp = table[idx];
                        while (true) {
                            if (tmp.next == null) {
                                tmp.next = node;
                                break;
                            }
                            tmp = tmp.next;
                        }
                    }
                    node = next;
                }
            }
        }
        threshold <<= 1;
        return table;
    }

    @Override
    public V get(Object key) {
        Node<K, V> e = getNode(key);
        return e == null ? null : e.value;
    }

    private Node<K, V> getNode(Object key) {
        int hash = hash(key);
        int n;
        Node<K, V> first;

        if (table != null && (n = table.length) > 0 && (first = table[(n - 1) & hash]) != null) {
            Node<K, V> tmp = first;
            while (tmp != null) {
                if (tmp.hash == hash && Objects.equals(key, tmp.key))
                    return tmp;
                tmp = tmp.next;
            }
        }
        return null;
    }

    @Override
    public Set<K> keySet() {
        Set<K> keys = keySet;
        if (keys == null) {
            keys = new HashSet<>();
        }
        return keys;
    }

    @Override
    public Collection<V> values() {
        List<V> vals = (List<V>) values;
        if (vals == null) {
            vals = new ArrayList<>();
        }
        return vals;
    }

    @Override
    public boolean containsKey(Object key) {
        return getNode(key) != null;
    }

    @Override
    public boolean containsValue(Object value) {
        Node<K, V>[] tab;
        V v;
        if ((tab = table) != null && size > 0) {
            for (Node<K, V> node : tab) {
                while (node != null) {
                    if (node.value.equals(value))
                        return true;

                    node = node.next;
                }
            }
        }
        return false;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public V remove(Object key) {
        int hash = hash(key);
        Node<K, V> kvNode = removeNode(hash, key);

        afterNodeDeleting(kvNode);


        return kvNode == null ? null : kvNode.value;
    }

    private Node<K, V> removeNode(int hash, Object key) {
        int idx, n;
        Node<K, V> nodeToDelete;
        if (table != null && (n = table.length) > 0 && table[idx = ((n - 1) & hash)] != null) {
            Node<K, V> node = table[idx];
            if (node.hash == hash && Objects.equals(key, node.key)) {
                nodeToDelete = node;
                table[idx] = node.next;
                size--;

                return nodeToDelete;

            } else {
                while (node.next != null) {
                    if (node.next.hash == hash && Objects.equals(key, node.next.key)) {
                        nodeToDelete = node;
                        node.next = node.next.next;
                        size--;

                        return nodeToDelete;
                    }
                    node = node.next;
                }
            }

        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        if (table != null && size > 0) {
            size = 0;
            Arrays.fill(table, null);
            values = null;
            keySet = null;
        }
    }
}
