package org.example;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class HashMapTest {

    private Map<String, Integer> map;

    @BeforeEach
    void setUp() {
        map = new HashMap<>();

        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);
        map.put("four", 4);
        map.put("five", 5);
    }

    @Test
    void createMapWithNegativeCapacity() {
        assertThrows(IllegalArgumentException.class, () -> map = new HashMap<>(-5));
    }


    @Test
    void put() {
        map.put("six", 6);

        assertEquals(6, map.get("six"));
        assertEquals(6, map.size());
        assertTrue(map.containsKey("six"));
        assertTrue(map.containsValue(6));
    }

    @Test
    void putWithExistKey() {
        map.put("five", 55);

        assertEquals(55, map.get("five"));
        assertEquals(5, map.size());
        assertTrue(map.containsKey("five"));
        assertTrue(map.containsValue(55));
    }


    @Test
    void putInEmptyMap() {
        map = new HashMap<>();
        map.put("one", 1);

        assertEquals(1, map.get("one"));
        assertEquals(1, map.size());
        assertTrue(map.containsKey("one"));
        assertTrue(map.containsValue(1));
    }

    @Test
    void putWithResize() {
        for (int i = 6; i < 100; i++) {
            map.put(String.format("%d", i), i);
        }
        assertEquals(50, map.get("50"));
        assertEquals(99, map.size());
    }

    @Test
    void get() {
        assertEquals(1, map.get("one"));
        assertEquals(5, map.get("five"));
    }

    @Test
    void keySet() {
        Set<String> keys = Set.of("one", "two", "three", "four", "five");
        Set<String> sampleKeys = map.keySet();
        assertEquals(keys, sampleKeys);
    }

    @Test
    void values() {
        List<Integer> values = List.of(1, 2, 3, 4, 5);

        List<Integer> sampleValues = (List<Integer>) map.values();
        Collections.sort(sampleValues);

        assertEquals(values, sampleValues);
    }

    @Test
    void containsKey() {
        assertTrue(map.containsKey("one"));
    }

    @Test
    void containsNotExistKey() {
        assertFalse(map.containsKey("zero"));
    }

    @Test
    void containsValue() {
        assertTrue(map.containsValue(1));
    }

    @Test
    void containsNotExistValue() {
        assertFalse(map.containsValue(0));
    }

    @Test
    void isEmpty() {
        assertFalse(map.isEmpty());
    }

    @Test
    void remove() {
        map.remove("five");

        assertEquals(4, map.size());
        assertNull(map.get("five"));
        assertFalse(map.containsKey("five"));
        assertFalse(map.containsValue(5));
    }

    @Test
    void removeNotExistKey() {
        assertNull(map.remove("zero"));
    }

    @Test
    void removeElementFromBucket() {
        map = new HashMap<>();
        for (int i = 0; i < 100; i++) {
            map.put(String.format("%d", i), i);
        }

        int size = 100;
        for (int i = 0; i < 100; i++) {
            String key = String.format("%d", i);
            map.remove(key);

            assertEquals(--size, map.size());
            assertNull(map.get(key));
        }
    }

    @Test
    void size() {
        assertEquals(5, map.size());
    }

    @Test
    void clear() {
        map.clear();
        assertTrue(map.isEmpty());
        assertEquals(0, map.size());
    }
}